#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument
#include <string>
#include <iostream>

// ---------
// Allocator
// ---------

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;
    }                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);
    }

public:
    // --------
    // typedefs
    // --------

    using      value_type = T;

    using       size_type = std::size_t;
    using difference_type = std::ptrdiff_t;

    using       pointer   =       value_type*;
    using const_pointer   = const value_type*;

    using       reference =       value_type&;
    using const_reference = const value_type&;

public:
    // ---------------
    // iterator
    // over the blocks
    // ---------------

    class iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const iterator& lhs, const iterator& rhs) {
            return lhs._p == rhs._p;
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const iterator& lhs, const iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        int* _p;

    public:
        // -----------
        // constructor
        // -----------

        iterator (int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        int& operator * () const {
            //static int* temp = this->_p;
            return *((int*)(((char*)_p) - 4));
        }

        // -----------
        // operator ++
        // -----------

        iterator& operator ++ () {
            iterator& temp = *this;
            long unsigned int n = *((int*)_p);
            _p = ((char*)_p) + n + 8;
            return temp;
        }

        // -----------
        // operator ++
        // -----------

        iterator operator ++ (int) {
            long unsigned int n = *((int*)_p);
            _p = (int*)(((char*)_p) + n + 8);
            return *this;
        }

        // -----------
        // operator --
        // -----------

        iterator& operator -- () {
            iterator& temp = *this;
            long unsigned int n = *((int*)_p - 4);
            _p = ((char*)_p) - n - 8;
            return temp;
        }

        // -----------
        // operator --
        // -----------

        iterator operator -- (int) {
            long unsigned int n = *((int*)_p - 4);
            _p = ((char*)_p) - n - 8;
            return *this;
        }
    };

    // ---------------
    // const_iterator
    // over the blocks
    // ---------------

    class const_iterator {
        // -----------
        // operator ==
        // -----------

        friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
            return (lhs._p == rhs._p);
        }

        // -----------
        // operator !=
        // -----------

        friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
            return !(lhs == rhs);
        }

    private:
        // ----
        // data
        // ----

        const int* _p;

    public:
        // -----------
        // constructor
        // -----------

        const_iterator (const int* p) {
            _p = p;
        }

        // ----------
        // operator *
        // ----------

        const int& operator * () const {
            // char* tmp = (char*)_p;
            // int val = *((int*)(tmp - 4));
            return *((int*)(((char*)_p) - 4));
        }                 // replace!

        // -----------
        // operator ++
        // -----------

        const_iterator& operator ++ () {
            long unsigned int n = *((int*)_p);
            _p = (int*)(((char*)_p) + n + 8);
            return *this;
        }

        // -----------
        // operator ++
        // -----------

        const_iterator operator ++ (int) {
            const_iterator tmp = *this;
            long unsigned int n = *((int*)_p);
            _p = (int*)(((char*)_p) + n + 8);
            return tmp;
        }

        // -----------
        // operator --
        // -----------

        const_iterator& operator -- () {
            long unsigned int n = *((int*)_p - 4);
            _p = (int*)(((char*)_p) - n - 8);
            return *this;
        }

        // -----------
        // operator --
        // -----------

        const_iterator operator -- (int) {
            const_iterator tmp = *this;
            long unsigned int n = *((int*)_p - 4);
            _p = (int*)(((char*)_p) - n - 8);
            return tmp;
        }
    };

private:
    // ----
    // data
    // ----

    char a[N];

    // -----
    // valid
    // -----

    /**
     * O(1) in space
     * O(n) in time
     * checking that current heap is valid - no adjacent free blocks and 
     */
    bool valid () const {
        const_iterator b = this->begin();
        const_iterator e = this->end();
        while(b != e) {
            int size = *b;
            //check that block is big enough
            if((long unsigned int)size < sizeof(T) && size > 0) {
                std::cout << "not valid" << std::endl;
                return false;
            }
            //check that two adjacent blocks aren't both free
            if((char*)(&(*b)) - &a[0] > 0) {
                const_iterator prev = --b;
                if(*prev > 0 && size > 0) {
                    std::cout << "not valid" << std::endl;
                    return false;
                }
                b++;
            }
            ++b;
        }
        std::cout << "valid" << std::endl;
        return true;
    }

public:
    // -----------
    // constructor
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
     */

    my_allocator () {
        if(N < sizeof(T) + 2 * sizeof(int)) {
            throw "bad_alloc exception";
        }
        char* head = &a[0];
        //set sentinels to N - 8 (taking size of sentinels into account when declaring free space)
        *((int*)head) = N - 8;
        *((int*)(head + 4 + N)) = N - 8;
    }

    my_allocator             (const my_allocator&) = default;
    ~my_allocator            ()                    = default;
    my_allocator& operator = (const my_allocator&) = default;

    // --------
    // allocate
    // --------


    /**
     * O(1) in space
     * O(n) in time
     * after allocation there must be enough space left for a valid block
     * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
     * choose the first block that fits
     * throw a bad_alloc exception, if n is invalid
     */
    pointer allocate (size_type n) {
        int alloc_size = n * 8;
        char* p = &a[0];

        //find free block
        bool found_free = false;
        int block_size = 0;
        while(!found_free && p < (&a[0] + 1000)) {
            block_size = *((int*)p);
            if(block_size >= alloc_size) {
                found_free = true;
            } else {
                if (block_size < 0) {
                    block_size *= -1;
                }
                p = p + block_size + 8;
            }
        }

        //check that p not out of bounds (heap has free block big enough)
        if(p >= &a[0] + 1000) {
            throw "bad alloc exception";
        }

        int rem = block_size - alloc_size - 8;

        if((long unsigned int)rem < sizeof(T) && rem > 0) {
            return nullptr;
        }

        //allocate block
        *((int*)p) = -1 * alloc_size;
        *((int*)(p + 4 + alloc_size)) = -1 * alloc_size;

        //set header and footer for remaining block
        char* rem_p = p + 8 + alloc_size;
        if(rem_p < &a[0] + 1000 && rem > 0) {
            *((int*)rem_p) = rem;
            *((int*)(rem_p + 4 + rem)) = rem;
        }
        assert(*((int*)p) < 0);
        return (pointer)p;

    }

    // ---------
    // construct
    // ---------

    /**
     * O(1) in space
     * O(1) in time
     */
    void construct (pointer p, const_reference v) {
        new (p) T(v);                               // this is correct and exempt
        assert(valid());
    }                           

    // ----------
    // deallocate
    // ----------

    /**
     * O(1) in space
     * O(1) in time
     * after deallocation adjacent free blocks must be coalesced
     * throw an invalid_argument exception, if p is invalid
     */
    void deallocate (pointer p, size_type n) {
        if((char*)p < (&a[0]) || (char*)p >= &a[0] + N) {
            throw "invalid argument";
        }

        char* dealloc_p = (char*)p;
        int dealloc_size = *((int*)p) * -1;

        //deallocate current block
        *((int*)dealloc_p) = dealloc_size;
        *((int*)(dealloc_p + 4 + dealloc_size)) = dealloc_size;

        //check if next block is free, if so combine it with block that was just deallocated
        char* next_p = dealloc_p + 8 + dealloc_size;
        if(next_p < (&a[0] + 1000) && *((int*)next_p) > 0) {
            int next_size = *((int*)next_p);
            dealloc_size += 8 + next_size;
            *((int*)dealloc_p) = dealloc_size;
            *((int*)(dealloc_p + 4 + dealloc_size)) = dealloc_size;
        }

        //check if prev block is free, if it is, combine it with block that was just deallocated
        char* prev_p = dealloc_p - 4;
        if(prev_p >= &a[0] && *((int*)prev_p) > 0) {
            int prev_size = *((int*)prev_p);
            prev_p = prev_p - prev_size - 4;
            dealloc_size += 8 + prev_size;
            *((int*)prev_p) = dealloc_size;
            *((int*)(prev_p + 4 + dealloc_size)) = dealloc_size;
            dealloc_p = prev_p;
        }

        assert(*((int*)dealloc_p) > 0);
    }

    // -------
    // destroy
    // -------

    /**
     * O(1) in space
     * O(1) in time
     */
    void destroy (pointer p) {
        p->~T();               // this is correct
        assert(valid());
    }

    // -----------
    // operator []
    // -----------

    /**
     * O(1) in space
     * O(1) in time
     */
    int& operator [] (int i) {
        return *reinterpret_cast<int*>(&a[i]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const int& operator [] (int i) const {
        return *reinterpret_cast<const int*>(&a[i]);
    }

    // -----
    // begin
    // -----

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator begin () {
        return iterator(&(*this)[0]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator begin () const {
        return const_iterator(&(*this)[0]);
    }

    // ---
    // end
    // ---

    /**
     * O(1) in space
     * O(1) in time
     */
    iterator end () {
        return iterator(&(*this)[N]);
    }

    /**
     * O(1) in space
     * O(1) in time
     */
    const_iterator end () const {
        return const_iterator(&(*this)[N]);
    }

    //print current heap
    void print_heap() {
        char* p = &a[0];
        while(p < (&a[0] + 1000)) {
            int size = *((int*)p);
            std::cout << size << " ";
            //make size positive so we move through heap correctly
            if(size < 0) {
                size *= -1;
            }
            p = p + 8 + size;
        }
        std::cout << "\n";
    }

    //find nth busy part to deallocate, where n = block
    void deallocate_block(int block) {
        char* p = &a[0];
        while(p < &a[0] + 1000 && block > 0) {
            int size = *((int*)p);
            if(size < 0) {
                block--;
                if(block == 0) {
                    break;
                }
            }
            if(size < 0) {
                size *= -1;
            }
            p = p + 8 + size;
        }
        //check that there is bust block to deallocate (p not out of bounds)
        if(p >= &a[0] + 1000)
            return;
        size_type n = *((size_type*)p);
        deallocate((pointer) p, (size_type)n);
    }

};