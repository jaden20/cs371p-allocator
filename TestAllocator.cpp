// -----------------
// TestAllocator.c++
// -----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/googletest/docs/primer.md
// https://github.com/google/googletest/blob/master/googletest/docs/advanced.md

// --------
// includes
// --------

#include <algorithm> // count
#include <cstddef>   // ptrdiff_t
#include <memory>    // allocator

#include "gtest/gtest.h"

#include "Allocator.hpp"

using namespace testing;

// -----------------
// AllocatorFixture1
// -----------------

template <typename T>
struct AllocatorFixture1 : Test {
    // ------
    // usings
    // ------

    using allocator_type = T;
    using value_type     = typename T::value_type;
    using size_type      = typename T::size_type;
    using pointer        = typename T::pointer;
    using iterator       = typename my_allocator<allocator_type, 100>::const_iterator;
};

using allocator_types_1 =
    Types<
    std::allocator<int>,
    std::allocator<double>,
    my_allocator<int,    100>,
    my_allocator<double, 100>>;

#ifdef __APPLE__
TYPED_TEST_CASE(AllocatorFixture1, allocator_types_1,);
#else
TYPED_TEST_CASE(AllocatorFixture1, allocator_types_1);
#endif

TYPED_TEST(AllocatorFixture1, test0) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 1;
    const value_type v = 2;
    const pointer    p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(*p, v);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(AllocatorFixture1, test1) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

// -----------------
// AllocatorFixture2
// -----------------

template <typename T>
struct AllocatorFixture2 : Test {
    // ------
    // usings
    // ------

    using allocator_type = T;
    using value_type     = typename T::value_type;
    using size_type      = typename T::size_type;
    using pointer        = typename T::pointer;
    using iterator       = typename my_allocator<allocator_type, 100>::const_iterator;
};

using allocator_types_2 =
    Types<
    my_allocator<int,    100>,
    my_allocator<double, 100>>;

#ifdef __APPLE__
TYPED_TEST_CASE(AllocatorFixture2, allocator_types_2,);
#else
TYPED_TEST_CASE(AllocatorFixture2, allocator_types_2);
#endif

TYPED_TEST(AllocatorFixture2, test0) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 1;
    const value_type v = 2;
    const pointer    p = x.allocate(s);
    if (p != nullptr) {
        x.construct(p, v);
        ASSERT_EQ(*p, v);
        x.destroy(p);
        x.deallocate(p, s);
    }
}

TYPED_TEST(AllocatorFixture2, test1) {
    using allocator_type = typename TestFixture::allocator_type;
    using value_type     = typename TestFixture::value_type;
    using size_type      = typename TestFixture::size_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type   x;
    const size_type  s = 10;
    const value_type v = 2;
    const pointer    b = x.allocate(s);
    if (b != nullptr) {
        pointer e = b + s;
        pointer p = b;
        try {
            while (p != e) {
                x.construct(p, v);
                ++p;
            }
        }
        catch (...) {
            while (b != p) {
                --p;
                x.destroy(p);
            }
            x.deallocate(b, s);
            throw;
        }
        ASSERT_EQ(std::count(b, e, v), ptrdiff_t(s));
        while (b != e) {
            --e;
            x.destroy(e);
        }
        x.deallocate(b, s);
    }
}

TYPED_TEST(AllocatorFixture2, test2) {
    using allocator_type = typename TestFixture::allocator_type;

    allocator_type x;
    ASSERT_EQ(x[0], 92);
}                                         // fix test

TYPED_TEST(AllocatorFixture2, test3) {
    using allocator_type = typename TestFixture::allocator_type;

    const allocator_type x;
    ASSERT_EQ(x[0], 92);
}                                         // fix test




TYPED_TEST(AllocatorFixture1, test21) {
    using allocator_type = typename TestFixture::allocator_type;
    using pointer        = typename TestFixture::pointer;
    using iterator       = typename my_allocator<allocator_type, 100>::const_iterator;

    allocator_type a;
    pointer x = a.allocate(2);
    iterator b = a.begin();
    ASSERT_EQ(x, &(*b));
    a.destroy(x);
    a.deallocate(x, 2);
}

TYPED_TEST(AllocatorFixture1, test22) {
    using allocator_type = typename TestFixture::allocator_type;
    using const_iterator       = typename my_allocator<allocator_type, 100>::const_iterator;

    allocator_type a;
    const_iterator e = a.end();
    ASSERT_EQ(&a[0], &(*e));
}

TYPED_TEST(AllocatorFixture1, test23) {
    using allocator_type = typename TestFixture::allocator_type;
    using pointer        = typename TestFixture::pointer;
    using iterator       = typename my_allocator<allocator_type, 100>::const_iterator;

    allocator_type a;
    iterator b = a.begin();
    pointer x = a.allocate(1);
    *x = 5;
    ASSERT_EQ(*x, *b);
    a.destroy(x);
    a.deallocate(x, 1);
}

TYPED_TEST(AllocatorFixture1, test24) {
    using allocator_type = typename TestFixture::allocator_type;
    using pointer        = typename TestFixture::pointer;
    using iterator       = typename my_allocator<allocator_type, 100>::const_iterator;

    allocator_type a;
    iterator b = a.begin();
    pointer x = a.allocate(1);
    pointer y = a.allocate(1);
    *x = 5;
    *y = 10;
    b++;
    ASSERT_EQ(*y, *b);
    a.destroy(x);
    a.deallocate(x, 1);
    a.destroy(y);
    a.deallocate(y, 1);
}

TYPED_TEST(AllocatorFixture1, test25) {
    using allocator_type = typename TestFixture::allocator_type;
    using iterator       = typename my_allocator<allocator_type, 100>::const_iterator;

    allocator_type a;
    iterator b = a.begin();
    iterator e = a.end();
    b++;
    ASSERT_EQ(*b, *e);
}

TYPED_TEST(AllocatorFixture1, test26) {
    using allocator_type = typename TestFixture::allocator_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type a;
    pointer x = a.allocate(5);
    char* p = (((char*)x) - 4);
    ASSERT_EQ(*((int*)p), -20);
    a.destroy(x);
    a.deallocate(x, 5);
}

TYPED_TEST(AllocatorFixture1, test27) {
    using allocator_type = typename TestFixture::allocator_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type a;
    pointer x = a.allocate(1);
    char* p = (((char*)x) + 4);
    ASSERT_EQ(*((int*)p), -4);
    a.destroy(x);
    a.deallocate(x, 1);
}

TYPED_TEST(AllocatorFixture1, test28) {
    using allocator_type = typename TestFixture::allocator_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type a;
    pointer x = a.allocate(2);
    pointer y = a.allocate(2);
    char* p = (((char*)x) - 4);
    a.deallocate(x, 2);
    ASSERT_EQ(*((int*)p), 8);
    
    a.destroy(x);
    a.deallocate(x, 1);
    a.destroy(y);
    a.deallocate(y, 2);
}

TYPED_TEST(AllocatorFixture1, test29) {
    using allocator_type = typename TestFixture::allocator_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type a;
    pointer x = a.allocate(2);
    pointer y = a.allocate(2);
    char* p = (((char*)y) - 4);
    a.deallocate(y, 2);
    ASSERT_EQ(*((int*)p), 8);
    
    a.destroy(x);
    a.deallocate(x, 1);
    a.destroy(y);
    a.deallocate(y, 2);
}

TYPED_TEST(AllocatorFixture2, test210) {
    using allocator_type = typename TestFixture::allocator_type;
    using pointer        = typename TestFixture::pointer;

    allocator_type a;

    pointer x = a.allocate(2);
    pointer y = a.allocate(2);

    a.deallocate(x, 2);
    a.deallocate(y, 2);

    ASSERT_EQ(a[0], 92);
}

