# CS371p: Object-Oriented Programming Allocator Repo

* Name: Jaden Hyde

* EID: jah8624

* GitLab ID: jadenh20

* HackerRank ID: jadenhyde20

* Git SHA: 29053099b6b08d4ef30e3d3b450a90788a349180

* GitLab Pipelines: https://gitlab.com/jaden20/cs371p-allocator/-/pipelines

* Estimated completion time: 25

* Actual completion time: 30

* Comments: N/A
