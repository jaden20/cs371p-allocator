// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout

#include "Allocator.hpp"

// ----
// main
// ----

int main() {
    int t;
    std::string input_line;
    getline(std::cin, input_line);
    t = stoi(input_line);
    //std::cout << t << endl;

    assert(t > 0 && t <= 100);

    getline(std::cin, input_line);
    //std::cout << input_line << endl;

    while(t > 0) {
        // std::cout << "t = " << t << std::endl;
        //initialize allocator
        my_allocator<int, 1000> my_alloc;
        //getline(std::cin, input_line);
        while(getline(std::cin, input_line)) {
            if(input_line == "") {
                break;
            }
            int x = stoi(input_line);
            // std::cout << x << std::endl;
            if(x > 0) {
                my_alloc.allocate(x);
            } else {
                my_alloc.deallocate_block(x * -1);
            }
            // my_alloc.print_heap();
            // getline(std::cin, input_line);
        }
        my_alloc.print_heap();
        // std::cout << "\n";
        t--;
    }
    // std::cout << "\n";
}